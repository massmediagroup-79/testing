<?php

namespace Tests\Browser;

use App\Model\Account;
use Faker\Provider\at_AT\Payment;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PaymentBrowserTest extends DuskTestCase
{
    private $account;
    private $sum;
    private $payment;

    public function setUp(): void
    {
        parent::setUp();
        $this->account = Account::first();
        $this->payment = Payment::first();
    }

    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testPaymentCreate()
    {
        $this->browse(function (Browser $browser) {
            $this->browse(function (Browser $browser) {

                $browser->visit('http://localhost:8000/payment/create')
                    ->type('sum', $this->sum)
                    ->select('account_id', $this->ac->id)
                    ->click('@save');
            });

            $this->assertDatabaseHas('payments', [
                'sum' => $this->sum,
                'account_id' => $this->account->id,
            ]);
        });
    }
}
