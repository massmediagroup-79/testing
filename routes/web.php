<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('account', 'AccountController', ['only' => [
    'index', 'create', 'store','show']])->names('account');

Route::resource('payment', 'PaymentController', ['except' => [
    'index','show']])->names('payment');
