@extends('layouts.app')

@section('content')
    @include('includes.result_messages')
    @if($payment->exists)
        <h1 class="my-4">Edit payment</h1>
        <form method="POST" action="{{ route('payment.update', $payment->id )}}">
            @method('PATCH')
            @else
                <h1 class="my-4">Create payment
                </h1>
                <form method="POST" action="{{ route('payment.store')}}">
                    @endif
                    @csrf
                    <div class="form-group">
                        <label for="name">Sum</label>
                        <input name="sum"
                               id="sum"
                               type="text"
                               class="form-control"
                               value="{{ old('sum', $payment->sum) }}">
                    </div>
                    <div class="form-group">
                        <label for="account_id">Account</label>
                        <select name="account_id"
                                id="account_id"
                                class="form-control"
                                placeholder="Select account"
                                required>
                            @foreach($account as $accountOption)
                                <option value="{{ $accountOption->id }}"
                                        @if($accountOption->id == $payment->account_id) selected @endif>
                                    Account: {{$accountOption->id}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success" dusk="save">Save</button>
                </form>
@endsection
