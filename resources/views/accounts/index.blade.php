@extends('layouts.app')
@section('content')
    @include('includes.result_messages')
    @foreach($accounts as $item )
        <div class="card mb-4">
            <div class="card-body">
                <a href="{{ route('account.show', $item->id)}}">
                    <h2 class="card-title">Account {{$item->id}}</h2>
                </a>
                <p class="card-text">Balance: {{$item->balance}}</p>
            </div>
            <div class="card-footer text-muted">
                Created: {{$item->created_at}}
            </div>
        </div>
    @endforeach
@endsection
