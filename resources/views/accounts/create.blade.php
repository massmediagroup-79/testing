@extends('layouts.app')

@section('content')
    @include('includes.result_messages')
    <h1 class="my-4">Create account
    </h1>
    <form method="POST" action="{{ route('account.store')}}">
        @csrf
        <button type="submit" class="btn btn-success">Сonfirm creation</button>
    </form>
@endsection
