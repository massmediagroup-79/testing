@extends('layouts.app')
@section('content')
    @include('includes.result_messages')
    <a class="btn btn-secondary" href="{{ route('payment.create')}}" role="button">Add
        payment</a>
    <div class="card mb-4">
        <div class="card-body">
            <h2 class="card-title">Account {{$account->id}}</h2>
            <p class="card-text">Balance: {{$account->balance}}</p>
        </div>
        <div class="card-footer text-muted">
            @foreach($account->payments as $item)
                Payment: {{$item->id}} -> sum = {{$item->sum}}
                <br>
                <a class="btn btn-secondary" href="{{ route('payment.edit',$item->id) }}"
                   role="button">Edit</a>
                <form method="POST" action="{{route('payment.destroy',$item->id) }}">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-secondary"
                            type="submit">Delete
                    </button>
                </form>
                <br>
            @endforeach
        </div>
    </div>
@endsection
