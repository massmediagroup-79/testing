<?php

namespace App\Http\Controllers;

use App\Model\Account;
use App\Model\Payment;
use App\Service\BalanceAccountService;
use App\Service\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends Controller
{
    /**
     * @var CreatePaymentService
     */
    private $balanceAccountService;

    /**
     * PaymentController constructor.
     * @param PaymentService $paymentService
     * @param BalanceAccountService $balanceAccountService
     */
    public function __construct(BalanceAccountService $balanceAccountService)
    {
        $this->balanceAccountService = $balanceAccountService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment.create_edit', ['payment' => new Payment(), 'account' => Account::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $item = Payment::create($request->input());
        if ($item) {
            $this->balanceAccountService->addOrSubSum($item, balanceAccountService::ADD);
            return redirect()->route('account.index')->with(['success' => 'Saved']);
        }
        return back()->withErrors(['msg' => 'Errors'])
            ->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Payment $payment
     * @return Response
     */
    public function edit(Payment $payment)
    {
        return view('payment.create_edit', ['payment' => $payment, 'account' => Account::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Payment $payment
     * @return Response
     */
    public function update(Request $request, Payment $payment)
    {
        $item = clone $payment;
        if ($payment->update($request->input())) {
            $this->balanceAccountService->newBalance($item, $payment);
            return back()->with(['success' => "Saved"]);
        }
        return back()->withErrors(['msg' => "Error"])
            ->withInput();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param Payment $payment
     * @return Response
     * @throws \Exception
     */
    public function destroy(Payment $payment)
    {
        if ($payment->delete()) {
            $this->balanceAccountService->addOrSubSum($payment, balanceAccountService::SUB);
            return back()->with(['success' => "Deleted"]);
        }
    }
}
