<?php

namespace App\Http\Controllers;

use App\Model\Account;
use App\Model\Payment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('accounts.index', ['accounts' => Account::with('payments')->get()]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $item = Account::create();
        if ($item) {
            return redirect()->route('account.index')->with(['success' => 'Saved']);
        }
        return back()->withErrors(['msg' => 'Errors'])
            ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param Account $account
     * @return Response
     */
    public function show(Account $account)
    {
        return view('accounts.show', compact('account'));
    }
}
