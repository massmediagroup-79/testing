<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'sum',
        'account_id',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
