<?php


namespace App\Service;

use App\Model\Account;

class BalanceAccountService
{
    const ADD = '+';
    const SUB = '-';
    /**
     * @param $data
     * @param $operation
     * @return mixed
     */
    public function addOrSubSum($data, $operation)
    {
        if ($operation === self::ADD) {
            $data->account->balance += $data->sum;
        } else {
            $data->account->balance -= $data->sum;
        }
        $data->account->save();
        return $data->account;
    }

    /**
     * @param $oldPayment
     * @param $newPayment
     */

    public function newBalance($oldPayment, $newPayment)
    {
            $this->addOrSubSum($oldPayment, self::SUB);
            $this->addOrSubSum($newPayment, self::ADD);
    }
}
