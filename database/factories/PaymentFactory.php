<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\Account;
use App\Model\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'sum' => $faker->randomNumber(),
        'account_id' => function () {
            $account = factory(Account::class)->create();
            return $account->id;
        }
    ];
});
